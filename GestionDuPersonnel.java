public class GestionDuPersonnel {
    public static void main(String[] args) {

   // Informations sur les employés
        String[] employeeNames = {"Alice Dupont", "Bob Martin", "Charlie Besson", "Diane Loriot", "Eva Joly"};

        int[] hoursWorked = {35, 38, 35, 38, 40};

        double[] hourlyRates = {12.5, 15.0, 13.5, 14.5, 13.0};

        String[] positions = {"Caissier", "Projectionniste", "Caissier", "Manager", "Caissier"};

    // Calculer et afficher le salaire hebdomadaire pour chaque employé si heures supplementaire taux horaire * 1.5.
    System.out.println("Salaire hebdomadaire des employés :");
        for (int i = 0; i < employeeNames.length; i++) {
            double heuresNormales;
            double heuresSupplementaires;
            double tauxSupplementaire;

            if (hoursWorked[i] > 35) {
                heuresNormales= 35;
                heuresSupplementaires = hoursWorked[i] - 35;
                tauxSupplementaire = hourlyRates[i] * 1.5;

            } else {
                heuresNormales = hoursWorked[i];
                heuresSupplementaires = 0;
                tauxSupplementaire = hourlyRates[i];
            }

        double salaireHebdomadaire = ( heuresNormales * hourlyRates[i]) + (heuresSupplementaires * tauxSupplementaire);

         System.out.println(employeeNames[i] + " (" + positions[i] + ") : " + salaireHebdomadaire + " €" );

        }
    //Boucle for pour lister tous les noms de tous les employés dont le poste correspond à la valeur de searchPosition, sinon affiche aucun employés 

    String[] searchPosition = {"Projectionniste"};
    
        System.out.println("Liste des employés qui ont le poste de Projectionniste " + " : ");
        boolean employeesProjectionnistes = false;
        for (int i = 0 ; i < employeeNames.length ; i++) {
            if (positions[i].equals(searchPosition[0])){

                System.out.println(employeeNames[i]);
                employeesProjectionnistes = true;
            }
        } if (!employeesProjectionnistes) {
                
                System.out.println("Aucun employé trouvé.");
                employeesProjectionnistes = false;
            }             
    }

}